export const delay = timeout => new Promise(resolve => setTimeout(resolve, timeout));

export const runPromisesInSeries = promiseCalls => {
  return promiseCalls.reduce((promise, next) => {
    return promise.then(next);
  }, Promise.resolve());
};

export const Promise_all = promises => {
  if (promises.length === 0) return Promise.resolve([]);
  return new Promise((resolve, reject) => {
    const results = [];
    let completedCounter = 0;
    promises.forEach((element, index) => {
      Promise.resolve(element)
        .then(result => {
          results[index] = result;
          completedCounter += 1;
          if (completedCounter === promises.length) {
            resolve(results);
          }
        })
        .catch(err => reject(err));
    });
  });
};

function* fib(n) {
  const infinite = !n && n !== 0;
  let current = 0;
  let next = 1;

  while (infinite || n--) {
    yield current;
    [current, next] = [next, current + next];
  }
}
export const fibonacci = n => fib(n);

export const helper = generator => {
  const iterator = generator();

  function run(arg) {
    const result = iterator.next(arg);
    if (result.done) {
      return result.value;
    } else {
      return Promise.resolve(result.value)
        .then(run)
        // prevent unhandled exception during test
        .catch(e => console.error('error happened', e.message || e));
    }
  }
  return run();
};

export const fetchingTheListOfComments = async () => {
  try {
    const getHeaders = options => {
      return {
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
        },
        ...options,
      };
    };
    const responseComments = await fetch(
      'https://jsonplaceholder.typicode.com/comments',
      getHeaders()
    );
    const comments = await responseComments.json();
    const randomNumber = Math.floor(Math.random() * comments.length);
    const randomComment = comments[randomNumber];
    const responsePost = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${randomComment.postId}`,
      getHeaders()
    );
    const post = await responsePost.json();
    const responseUser = await fetch(
      `https://jsonplaceholder.typicode.com/users/${post.userId}`,
      getHeaders()
    );
    const user = await responseUser.json();
    await fetch(
      `https://jsonplaceholder.typicode.com/posts/${post.id}`,
      getHeaders({
        method: 'DELETE',
      })
    );
    const changeNameResponse = await fetch(
      `https://jsonplaceholder.typicode.com/users/${user.id}`,
      getHeaders({
        method: 'PATCH',
        body: JSON.stringify({
          name: 'Hello World',
        }),
      })
    );
    console.log(await changeNameResponse.json());
  } catch (e) {
    console.log('Error', e);
  }
};

export function MyPromise(executor) {
  let onResolve, onReject;
  let fulfilled = false,
    rejected = false,
    called = false,
    value;

  function resolve(v) {
    fulfilled = true;
    value = v;
    if (typeof onResolve === 'function') {
      onResolve(value);
      called = true;
    }
  }

  function reject(reason) {
    rejected = true;
    value = reason;
    if (typeof onReject === 'function') {
      onReject(value);
      called = true;
    }
  }

  this.then = function(callback) {
    onResolve = callback;
    if (fulfilled && !called) {
      called = true;
      onResolve(value);
    }
    return this;
  };

  this.catch = function(callback) {
    onReject = callback;
    if (rejected && !called) {
      called = true;
      onReject(value);
    }
    return this;
  };

  try {
    executor(resolve, reject);
  } catch (error) {
    reject(error);
  }
}

MyPromise.resolve = val =>
  new MyPromise(function executor(resolve) {
    resolve(val);
  });

MyPromise.reject = reason =>
  new MyPromise(function executor(_, reject) {
    reject(reason);
  });

MyPromise.all = promises => {
  const fulfilledPromises = [];
  const result = [];

  function executor(resolve, reject) {
    promises.forEach((promise, index) =>
      promise
        .then(res => {
          fulfilledPromises.push(true);
          result[index] = res;

          if (fulfilledPromises.length === promises.length) {
            return resolve(result);
          }
        })
        .catch(error => {
          return reject(error);
        })
    );
  }

  return new MyPromise(executor);
};
