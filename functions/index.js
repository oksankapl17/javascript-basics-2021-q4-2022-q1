/**
 * Currying
 */
export const mergeWords = (str) => (nextStr) =>
  (nextStr === undefined ? str : mergeWords(`${str} ${nextStr}`));

/**
 * Every/Some
 */
export const checkUsersValid = (validUsers) => {
  return function (testUsers) {
    const check = (user) => validUsers.some((u) => u.id === user.id);
    return testUsers.every(check);
  };
};
/**
 * Reduce
 */

export const countWords = (arr) => {
  return arr.reduce((obj, el) => {
    obj[el] = (obj[el] || 0) + 1;
    return obj;
  }, {});
};

/**
 * Palindrome
 */
export const isPalindrome = (str) =>
  (str === str.split('').reverse().join('') ? 'The entry is a palindrome' : 'Entry is not a palindrome');

/**
 * Recursion
 */
export const factorial = (number) =>
  (number <= 1 ? 1 : number * factorial(number - 1));

export const amountToCoins = (amount, coins) => {
  if (amount === 0) {
    return [];
  } else {
    if (amount >= coins[0]) {
      const first = amount - coins[0];
      return [coins[0], ...amountToCoins(first, coins)];
    } else {
      coins.shift();
      return amountToCoins(amount, coins);
    }
  }
};

export const repeat = (func, num) => {
  if (num > 0) {
    func();
    return repeat(func, --num);
  }
};

export const reduce = ([first, ...last], callback, acc) => {
  if (first === undefined) {
    return acc;
  }
  return reduce(last, callback, callback(acc, first));
};
