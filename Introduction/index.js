/**
 * Change the capitalization of all letters in a string
 */
export const changeCase = str =>
  [...str]
    .map(el => (/[A-Z]/.test(el) ? el.toLowerCase() : el.toUpperCase()))
    .join('');

/**
 * Filter out the non-unique values in an array
 */
export const filterNonUnique = arr =>
  arr.filter(el => arr.indexOf(el) === arr.lastIndexOf(el));

/**
 * Sort string in alphabetical order
 */
export const alphabetSort = str => [...str].sort().join('');

/**
 * Get min integer
 */
export const getSecondMinimum = arr => {
  arr.sort((a, b) => a - b);
  return arr[1];
};

/**
 * Double every even integer
 */
export const doubleEveryEven = arr =>
  arr.map(el => (el % 2 === 0 ? el * 2 : el));

/**
 * Create array with all possible pairs of two arrays
 */
export const getArrayElementsPairs = (firstArr, secondArr) => {
  const result = [];
  for (let i = 0; i < firstArr.length; i++) {
    for (let j = 0; j < secondArr.length; j++) {
      result.push([firstArr[i], secondArr[j]]);
    }
  }
  return result;
};

/**
 * Deep equal
 */
export const deepEqual = (firstObj, secondObj) => {
  function isObject(object) {
    return object !== null && typeof object === 'object';
  }
  const keys1 = Object.keys(firstObj);
  const keys2 = Object.keys(secondObj);
  if (keys1.length !== keys2.length) {
    return false;
  }
  for (const key of keys1) {
    const val1 = firstObj[key];
    const val2 = secondObj[key];
    const areObjects = isObject(val1) && isObject(val2);
    if (
      (areObjects && !deepEqual(val1, val2)) ||
      (!areObjects && val1 !== val2)
    ) {
      return false;
    }
  }
  return true;
};

/**
 * Format date
 */
export const formatDate = date => {
  if (!(date instanceof Date)) {
    if (Array.isArray(date)) {
      date = new Date(...date);
    } else {
      date = new Date(date);
    }
  }
  const year = String(date.getFullYear()).substr(-2);
  let month = String(date.getMonth() + 1);
  let day = String(date.getDate());
  if (month.length < 2) {
    month = `0${month}`;
  }
  if (day.length < 2) {
    day = `0${day}`;
  }

  return `${day}.${month}.${year}`;
};
