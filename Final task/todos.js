import { formatDate, randomGen, validateDate } from './utils';

export default class Todos {
  constructor() {
    this.todos = JSON.parse(localStorage.getItem('data') || '[]') || [];
    this.openContainer = document.querySelector('ul.openList');
    this.doneContainer = document.querySelector('ul.doneList');
  }

  addTodo(name) {
    const newTodo = {
      name,
      completed: false,
      completedAt: null,
      createdAt: new Date(),
      id: randomGen(),
    };
    this.todos = [...this.todos, ...[newTodo]];
  }

  toggle({ target }) {
    this.todos = this.todos.map(t => {
      if (target.dataset.key === t.id) {
        t.completed = !t.completed;
        t.completedAt = t.completed ? new Date() : null;
      }
      return t;
    });
    localStorage.setItem('data', JSON.stringify(this.todos));
    this.render();
  }

  deleteTodo({ target }) {
    this.todos = this.todos.filter(t => target.dataset.key !== t.id);
    localStorage.setItem('data', JSON.stringify(this.todos));
    this.render();
  }

  editItem(todo, li, p) {
    const editInput = document.createElement('input');
    editInput.classList.add('editInput');
    li.replaceChild(editInput, p);
    editInput.value = todo.name;
    editInput.focus();

    const onBlur = ({ target }) => {
      todo.name = target.value;
      localStorage.setItem('data', JSON.stringify(this.todos));
      this.render();
    };

    const onKeyUp = ({ key, target }) => {
      if (key === 'Enter') {
        todo.name = target.value;
        localStorage.setItem('data', JSON.stringify(this.todos));
        this.render();
      }
      if (key === 'Escape') {
        editInput.removeEventListener('blur', onBlur);
        this.render();
      }
    };

    editInput.addEventListener('keyup', onKeyUp);
    editInput.addEventListener('blur', onBlur);
  }

  renderParagraph(todo, li) {
    const paragraph = document.createElement('span');
    paragraph.classList.add('para-1');
    paragraph.textContent = todo.name;
    paragraph.addEventListener('dblclick', () => this.editItem(todo, li, paragraph));

    return paragraph;
  }

  renderDate(todo) {
    const div = document.createElement('div');
    const createdDate = document.createElement('p');
    const createdAt = validateDate(todo.createdAt);
    const completedAt = validateDate(todo.completedAt);
    createdDate.textContent = formatDate(createdAt);
    const doneDate = document.createElement('p');
    doneDate.textContent = todo.completedAt ? formatDate(completedAt) : '';
    div.append(createdDate, doneDate);
    return div;
  }

  renderCheckbox(todo) {
    const input = document.createElement('input');
    input.setAttribute('type', 'checkbox');
    input.checked = todo.completed;
    input.setAttribute('data-key', todo.id);
    input.classList.add('status');
    input.addEventListener('click', this.toggle.bind(this));
    return input;
  }

  renderDeleteButton({ id }) {
    const btn = document.createElement('button');
    btn.setAttribute('data-key', id);
    btn.classList.add('deleteBtn');
    btn.addEventListener('click', this.deleteTodo.bind(this));
    return btn;
  }

  renderListItem(todo) {
    const li = document.createElement('li');
    li.classList.add('li');
    const checkbox = this.renderCheckbox(todo);
    const p = this.renderParagraph(todo, li);
    const dates = this.renderDate(todo);
    const deleteBtn = this.renderDeleteButton(todo);
    li.append(checkbox, p, dates, deleteBtn);
    return li;
  }

  validateLists() {
    [this.openContainer, this.doneContainer].forEach(c => {
      if (c.children.length === 0) {
        const li = document.createElement('li');
        li.classList.add('li');
        li.textContent = 'No tasks';
        c.append(li);
      }
    });
  }

  resetHTML() {
    this.openContainer.innerHTML = '';
    this.doneContainer.innerHTML = '';
  }

  renderWithCondition(filter) {
    // {  completed: boolean; prop: 'name'|'completedAt|'createdAt'; direction: 'asc'|'desc' }
    const { prop, direction, completed } = filter;
    const isAsc = direction === 'asc';
    let filteredTodos = this.todos.filter(i => i.completed === completed);

    function compareDates(a, b) {
      const first = isAsc ? validateDate(a[prop]) : validateDate(b[prop]);
      const next = isAsc ? validateDate(b[prop]) : validateDate(a[prop]);
      return first.getTime() - next.getTime();
    }

    if (prop !== 'name') {
      filteredTodos = filteredTodos.sort(compareDates);
    } else {
      filteredTodos = filteredTodos.sort((a, b) =>
        isAsc ? a.name.localeCompare(b.name) : b.name.localeCompare(a.name)
      );
    }
    const container = completed ? this.doneContainer : this.openContainer;
    container.innerHTML = '';
    filteredTodos.forEach(todo => {
      const listItem = this.renderListItem(todo);
      container.append(listItem);
    });
  }

  render() {
    this.resetHTML();
    this.todos.forEach(todo => {
      const listItem = this.renderListItem(todo);
      const container = todo.completed ? this.doneContainer : this.openContainer;
      container.append(listItem);
    });
    this.validateLists();
    const itemsLeft = document.querySelector('.items-left');
    itemsLeft.textContent = String(this.todos.filter(t => t.completed === false).length);
  }
}
