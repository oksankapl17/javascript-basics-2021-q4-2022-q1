export const randomGen = (str = 'qwertyuio') =>
  `${str
    .split('')
    .sort(() => Math.random() - 0.5)
    .join('')}_${Math.floor(Math.random() * 1000)}`;

export const formatDate = (date = new Date()) =>
  date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

export const validateDate = dateOrString =>
  (dateOrString instanceof Date ? dateOrString : new Date(dateOrString));

export const debounce = (func, wait) => {
  let timeout;

  return function executedFunction(...args) {
    const later = () => {
      clearTimeout(timeout);
      func(...args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};
