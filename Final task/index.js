import './style.css';
import Todos from './todos';
import { debounce } from './utils';

// Access elements
const mainSearch = document.querySelector('#searchTodos');
const mainInput = document.querySelector('.mainInput');
const openFilter = document.querySelector('#openFilter');
const doneFilter = document.querySelector('#doneFilter');
const clearOpenBtn = document.querySelector('#clearOpenList');
const clearDoneBtn = document.querySelector('#clearDoneList');
const addTaskBtn = document.querySelector('#addTask');

const handleFilter = (value, completed) => {
  if (!value) {
    return allTasks.render();
  }
  const [prop, direction] = value.split('-');
  const tasks = new Todos();
  tasks.renderWithCondition({ completed, prop, direction });
};

const handleOpenFilter = ({ target }) => {
  handleFilter(target.value, false);
};

const handleDoneFilter = ({ target }) => {
  handleFilter(target.value, true);
};

const clearList = condition => {
  allTasks.todos = allTasks.todos.filter(condition);
  localStorage.setItem('data', JSON.stringify(allTasks.todos));
  allTasks.render();
};
const clearOpenList = () => {
  const condition = t => t.completed;
  clearList(condition);
};

const clearDoneList = () => {
  const condition = t => !t.completed;
  clearList(condition);
};

const handleSearch = () => {
  const filtered = new Todos();
  const query = mainSearch.value;
  filtered.todos = allTasks.todos.filter(t => t.name.indexOf(query) >= 0);
  filtered.render();
};

const addNewTodo = ({ target, key }) => {
  if (key === 'Enter' || target.id === 'addTask') {
    allTasks.addTodo(target.value || mainInput.value);
    localStorage.setItem('data', JSON.stringify(allTasks.todos));
    mainInput.value = '';
    allTasks.render();
  }
};

// Init listeners
mainSearch.addEventListener('keyup', debounce(handleSearch, 500));
mainInput.addEventListener('keyup', addNewTodo);
addTaskBtn.addEventListener('click', addNewTodo);
openFilter.addEventListener('change', handleOpenFilter);
doneFilter.addEventListener('change', handleDoneFilter);
clearOpenBtn.addEventListener('click', clearOpenList);
clearDoneBtn.addEventListener('click', clearDoneList);
// Init tasks
const allTasks = new Todos();
allTasks.render();
