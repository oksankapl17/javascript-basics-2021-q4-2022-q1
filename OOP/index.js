/**
 * Point
 *
 * @param {number} x
 * @param {number} y
 */
export class Point {

  constructor(x, y) {
    if (!(x && y)) {
      throw new Error('Please provide correct params.');
    }
    this.x = x;
    this.y = y;
  }

  plus(point) {
    return new Point(this.x + point.x, this.y + point.y);
  }
}

/**
 * Speaker and Screamer ES5
 */
export function SpeakerES5(name) {
  if (!name) {
    throw new Error('Please provide correct name.');
  }
  this.name = name;
  this.speak = function(phrase) {
    console.log(`${this.name} says ${phrase}`);
  };
  return this;
}

export function ScreamerES5(name) {
  SpeakerES5.call(this, name);
  this.speak = function(phrase) {
    console.log(`${this.name} shouts ${phrase.toUpperCase()}`);
  };
  return this;
}

/**
 * Speaker and Screamer ES6
 */
export class SpeakerES6 {
  constructor(name) {
    if (!name) {
      throw new Error('Please provide correct name.');
    }
    this.name = name;
  }

  speak(phrase) {
    console.log(`${this.name} says ${phrase}`);
  }
}

export class ScreamerES6 extends SpeakerES6 {
  constructor(name) {
    super(name);
  }

  speak(newPhrase) {
    console.log(`${this.name} shouts ${newPhrase.toUpperCase()}`);
  }
}

/**
 * The Reading list
 */
export class BookList {
  constructor() {
    this._currentBookIndex = 0;
    this._lastBookIndex = -1;
    this._books = [];
  }

  get booksFinished() {
    return this._books.filter(book => book.isRead).length || 0;
  }

  get booksNotFinished() {
    return this._books.filter(book => !book.isRead).length || 0;
  }

  get nextBook() {
    return this._books[this._currentBookIndex + 1] || null;
  }

  get currentBook() {
    return this._books[this._currentBookIndex] || null;
  }

  get lastBook() {
    return this._books[this._lastBookIndex] || null;
  }

  get books() {
    return this._books;
  }

  add(book) {
    if (book instanceof Book) {
      this._books.push(book);
    } else {
      throw new Error('Please provide Book instance.');
    }
  }

  finishCurrentBook() {
    this.currentBook.markAsRead();
    this._lastBookIndex = this._books.findIndex(book => this.currentBook === book);
    this._currentBookIndex = this._lastBookIndex + 1;
  }
}

export class Book {
  constructor({title, genre, author, isRead = false, dateFinished = null}) {
    if (!title) {
      throw new Error('Please provide book title.');
    }
    this.title = title;
    this.genre = genre;
    this.author = author;
    this.isRead = isRead;
    this.dateFinished = dateFinished;
  }

  markAsRead() {
    this.isRead = true;
    this.dateFinished = new Date(Date.now());
  }
}
