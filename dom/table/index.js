const tableBody = document.querySelector('#tbody');
let counter = 0;

function addCell() {
  counter++;
  const cell = document.createElement('td');
  const cellNameText = document.createTextNode(`Cell ${counter}`);
  cell.id = `userInfo${counter}`;
  cell.className = 'userInfo';
  cell.appendChild(cellNameText);
  return cell;
}

function addDeleteCell() {
  const cell = document.createElement('td');
  const cellDeleteIcon = document.createElement('icon');
  cellDeleteIcon.className = 'fa fa-trash deleteIcon';
  cell.appendChild(cellDeleteIcon);
  return cell;
}

function addInput(cell) {
  const input = document.createElement('input');
  input.type = 'text';
  input.id = 'textInput';
  input.value = cell.innerText;
  input.className = 'userInput';
  input.onkeydown = e => {
    if (e && e.code === 'Enter') {
      cell.innerHTML = document.getElementById('textInput').value;
    }
  };
  cell.innerHTML = '';
  cell.appendChild(input);
  input.focus();
}

function addRow() {
  const row = document.createElement('tr');
  [addCell(), addCell(), addDeleteCell()].forEach(cell => row.appendChild(cell));
  tableBody.appendChild(row);
}

function setEditListener(e) {
  if (!e.target.classList.contains('userInfo')) {
    return;
  }
  addInput(e.target);
}

function setDelListener(e) {
  if (e && e.target) {
    if (!e.target.classList.contains('deleteIcon')) {
      return;
    }
    const icon = e.target;
    icon.closest('tr').remove();
    counter = counter - 2;
  }
}

tableBody.addEventListener('click', setDelListener);
tableBody.addEventListener('dblclick', setEditListener);
