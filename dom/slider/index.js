const DEFAULT_INTERVAL = 2000;

const addUrlInput = document.querySelector('#add_url');
const addTimer = document.querySelector('#add_timer');
const slideshowContainer = document.querySelector('#slideshow_container');
const newImg = document.createElement('img');

slideshowContainer.appendChild(newImg);
const arrOfSrc = [];
let timerId;
let index = 0;
let interval = DEFAULT_INTERVAL;

function startSlider() {
  timerId = setInterval(() => {
    index = getIndex(index + 1);
    newImg.src = arrOfSrc[index];
  }, interval || DEFAULT_INTERVAL);
}

function addImage() {
  newImg.src = addUrlInput.value;
  arrOfSrc.push(addUrlInput.value);
  resetTimeout();
  addUrlInput.value = '';
}

function resetTimeout() {
  interval = +(addTimer.value * 1000);
  clearInterval(timerId);
  startSlider();
}

function getIndex(currentIndex) {
  if (index + currentIndex < 0) {
    return arrOfSrc.length - 1;
  } else if (index + currentIndex > arrOfSrc.length - 1) {
    return 0;
  } else {
    return index + currentIndex;
  }
}

function plusSlides(num) {
  index = getIndex(num);
  newImg.src = arrOfSrc[index];
  resetTimeout();
}

newImg.addEventListener('dblclick', e => {
  const shouldDelete = window.confirm('Are You really want to delete this image?');
  if (shouldDelete) {
    const imgIndex = arrOfSrc.findIndex(imgSrc => imgSrc === e.target.src);
    if (imgIndex >= 0) {
      arrOfSrc.splice(imgIndex, 1);
      if (arrOfSrc.length === 0) {
        clearInterval(timerId);
        e.target.src = '';
        index = 0;
      } else {
        index = getIndex(index + 1);
        e.target.src = arrOfSrc[index];
      }
    }
  }
});
