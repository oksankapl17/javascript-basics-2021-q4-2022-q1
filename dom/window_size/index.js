let id;

function getWindowSize() {
  clearTimeout(id);
  id = setTimeout(doneResizing, 400);
}


function doneResizing() {
  const heightEl = document.querySelector('#height');
  const widthEl = document.querySelector('#width');
  if (heightEl) {
    heightEl.innerHTML = String(window.innerHeight);
  }
  if (widthEl) {
    widthEl.innerText =  String(window.innerWidth);
  }
}
doneResizing();
