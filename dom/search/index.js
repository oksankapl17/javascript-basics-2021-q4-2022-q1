const userText = document.querySelector('#user_text');
const copiedText = document.querySelector('#copied_text');
const userInput = document.querySelector('#user_input');

function bold () {
  const changedCopiedTextArray = String(copiedText.innerHTML).split(' ');
  if (userInput.value) {
    const arrOfIndexes = [];
    let idx = changedCopiedTextArray.indexOf(userInput.value);
    while (idx !== -1) {
      arrOfIndexes.push(idx);
      idx = changedCopiedTextArray.indexOf(userInput.value, idx + 1);
    }
    for (let i = 0; i < arrOfIndexes.length; i++) {
      changedCopiedTextArray[arrOfIndexes[i]] = `<b>${changedCopiedTextArray[arrOfIndexes[i]]}</b>`;
    }
    copiedText.innerHTML = changedCopiedTextArray.join(' ');
  }
}

function copyText () {
  // your code here
  if (userText.value) {
    copiedText.innerHTML = userText.value;
  }
}
