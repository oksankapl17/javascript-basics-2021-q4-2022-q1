/**
 * Array to List
 */
export const arrayToList = ([first, ...rest]) => {
  if (!first) {
    return null;
  }
  return {
    value: first,
    rest: arrayToList(rest),
  };
};

export const listToArray = (obj) => {
  if (!obj) {
    return [];
  }
  return [obj.value, ...listToArray(obj.rest)];
};

/**
 * Keys and values to list
 */
export const getKeyValuePairs = (obj) => Object.entries(obj);

/**
 * Invert keys and values
 */
export const invertKeyValue = (obj) =>
  Object.fromEntries(Object.entries(obj).map(([key, value]) => [value, key]));

/**
 * Get all methods from object
 */
export const getAllMethodsFromObject = (obj) =>
  Object.getOwnPropertyNames(obj).filter(
    (property) => typeof obj[property] === 'function'
  );

/**
 * Clock
 */
export function Clock() {
  this.date = new Date();
}

Clock.prototype.run = function () {
  this.timerId = setInterval(() => {
    this.date.setSeconds(this.date.getSeconds() + 1);
    console.log(this.date.toLocaleTimeString());
  }, 1000);
};

Clock.prototype.stop = function () {
  clearInterval(this.timerId);
};
const clock = new Clock();
clock.run();

/**
 * Groups
 */
export class Group {
  constructor(groupValue) {
    this.group = [];
    if (!this.has(groupValue)) {
      this.group.push(groupValue);
    }
  }

  static from(array) {
    const group = new Group();
    array.forEach((i) => group.add(i));
    return group;
  }

  has(value) {
    return this.group.includes(value);
  }

  add(number) {
    const exists = this.group.find((el) => el === number);
    if (!exists) {
      this.group.push(number);
    }
  }

  delete(number) {
    const index = this.group.findIndex((el) => el === number);
    if (index >= 0) {
      this.group.splice(index, 1);
    }
  }
}
